/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.factory.annotation.Autowired;

public class LifecycleBeanConfigParam {

    private final Printer printer;

    @Autowired
    public LifecycleBeanConfigParam(Printer printer) {
        this.printer = printer;
        printer.println("LifecycleBeanConfigParam() constructor called!");
    }

    public void initMethod() {
        printer.println("LifecycleBeanConfigParam::initMethod() called!");
    }

    public void destroyMethod () {
        printer.println("LifecycleBeanConfigParam::destroyMethod() called!");
    }

    @Override
    public String toString() {
        return "Bean of LifecycleBeanConfigParam type.";
    }
}
