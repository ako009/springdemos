/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

// http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-factory-lifecycle-initializingbean
// http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-factory-lifecycle-disposablebean

public class LifecycleBeanInterface implements InitializingBean, DisposableBean {

    private final Printer printer;

    @Autowired
    public LifecycleBeanInterface(Printer printer) {
        this.printer = printer;
        printer.println("LifecycleBeanInterface() constructor called!");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        printer.println("LifecycleBeanInterface::afterPropertiesSet() called!");
    }

    @Override
    public void destroy() throws Exception {
        printer.println("LifecycleBeanInterface::destroy() called!");
    }

    @Override
    public String toString() {
        return "Bean of LifecycleBeanInterface type.";
    }
}
