/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.example;

import pl.korzechowski.springdemos.framework.BaseExample;
import pl.korzechowski.springdemos.framework.ExampleConfiguration;
import pl.korzechowski.springdemos.framework.LoggerPrinter;
import pl.korzechowski.springdemos.framework.AnnotationConfiguration;
import pl.korzechowski.springdemos.ioc.examples.lifecycle.configuration.LifecycleContextConfiguration;

public class ContextLifecycleExample extends BaseExample {

    public ContextLifecycleExample(ExampleConfiguration configuration) {
        super(configuration);
    }

    public static void main(String[] args) {
        ExampleConfiguration configuration = new AnnotationConfiguration(
                "Context Lifecycle via Configuration annotation",
                new LoggerPrinter("AnnotationAppCtx"),
                LifecycleContextConfiguration.class);

        new ContextLifecycleExample(configuration)
                .run();
    }

    @Override
    public void start() {
        // http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-factory-shutdown
        getContext().registerShutdownHook();

        //LifecycleContextLifecycleInterface usingLifecycleInterface = getBean("usingLifecycleInterface", LifecycleContextLifecycleInterface.class);
        //DefaultLifecycleProcessor lifecycleProcessor = getBean("lifecycleProcessor", DefaultLifecycleProcessor.class);

        printer.println("Calling start() on the 'lifecycleProcessor' bean.");
        //lifecycleProcessor.start();
        getContext().start();
        printer.println("start() returned from the 'lifecycleProcessor' bean.");
    }

}
