/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


// http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-factory-lifecycle-combined-effects
// http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/beans/factory/BeanFactory.html

public class LifecycleBeanAll implements InitializingBean, DisposableBean {

    private final Printer printer;
    private int someValue = 0;

    @Autowired
    public LifecycleBeanAll(Printer printer) {
        this.printer = printer;
        printer.println("LifecycleBeanAll() constructor called!");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        someValue = 2;
        printer.println("LifecycleBeanAll::afterPropertiesSet() called!");
    }

    @PostConstruct
    public void onInitialize() {
        someValue = 1;
        printer.println("LifecycleBeanAll::onInitialize() called!");
    }

    public void initMethod() {
        someValue = 3;
        printer.println("LifecycleBeanAll::initMethod() called!");
    }

    @Override
    public void destroy() throws Exception {
        printer.println("LifecycleBeanAll::destroy() called!");
    }

    @PreDestroy
    public void onDestroy () {
        printer.println("LifecycleBeanAll::onDestroy() called!");
    }

    public void destroyMethod () {
        printer.println("LifecycleBeanAll::destroyMethod() called!");
    }

    @Override
    public String toString() {
        return "Bean of LifecycleBeanAll type.";
    }

}
