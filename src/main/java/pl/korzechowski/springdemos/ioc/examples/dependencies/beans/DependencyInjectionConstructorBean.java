/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.dependencies.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;

public class DependencyInjectionConstructorBean implements BeanNameAware {

    private final Printer printer;
    private String beanName;
    private ServiceInterface someService;

    @Autowired
    public DependencyInjectionConstructorBean(Printer printer, ServiceInterface someService) {
        this.printer = printer;
        this.someService = someService;
        printer.println("DependencyInjectionConstructorBean() constructor called!");
        printer.println("DependencyInjectionConstructorBean() constructor someService has been injected!");
    }

    @Override
    public void setBeanName(String name) {
        printer.println("DependencyInjectionConstructorBean::setBeanName() called with '" + name + "' name.");
        beanName = name;
    }

    public void doSomething() {
        printer.println("DependencyInjectionConstructorBean::doSomething() called for bean '" + beanName + "'.");
        printer.println("DependencyInjectionConstructorBean::doSomething() calling someService.performSomeOperation().");
        someService.performSomeOperation();
        printer.println("DependencyInjectionConstructorBean::doSomething() returned from someService.performSomeOperation().");
    }

    @Override
    public String toString() {
        return "Bean of DependencyInjectionConstructorBean type.";
    }

}
