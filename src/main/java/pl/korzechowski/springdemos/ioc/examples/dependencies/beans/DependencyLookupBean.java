/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.dependencies.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class DependencyLookupBean implements ApplicationContextAware, BeanNameAware {

    private static final String serviceBeanName = "someService";
    private final Printer printer;
    private String beanName;
    private ApplicationContext applicationContext;
    private ServiceInterface someService;

    @Autowired
    public DependencyLookupBean(Printer printer) {
        this.printer = printer;
        printer.println("DependencyLookupBean() constructor called!");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        printer.println("DependencyLookupBean::setApplicationContext() called for bean '" + beanName + "'.");
        this.applicationContext = applicationContext;
        printer.println("DependencyLookupBean::setApplicationContext() getting bean '" + serviceBeanName + "'.");
        this.someService = applicationContext.getBean(serviceBeanName, ServiceInterface.class);
    }

    @Override
    public void setBeanName(String name) {
        printer.println("DependencyLookupBean::setBeanName() called with '" + name + "' name.");
        beanName = name;
    }

    public void doSomething() {
        printer.println("DependencyLookupBean::doSomething() called for bean '" + beanName + "'.");
        printer.println("DependencyLookupBean::doSomething() calling someService.performSomeOperation().");
        someService.performSomeOperation();
        printer.println("DependencyLookupBean::doSomething() returned from someService.performSomeOperation().");
    }

    @Override
    public String toString() {
        return "Bean of DependencyLookupBean type.";
    }

}
