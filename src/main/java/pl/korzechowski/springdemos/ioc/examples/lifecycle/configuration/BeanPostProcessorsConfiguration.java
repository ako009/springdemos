/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.configuration;

import pl.korzechowski.springdemos.framework.Printer;
import pl.korzechowski.springdemos.ioc.examples.lifecycle.beans.SampleBeanPostProcessor;
import pl.korzechowski.springdemos.ioc.examples.lifecycle.beans.LifecycleBeanAll;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanPostProcessorsConfiguration {

    @Bean(initMethod = "initMethod", destroyMethod = "destroyMethod")
    public LifecycleBeanAll allInOne(Printer printer) {
        return new LifecycleBeanAll(printer);
    }

    @Bean
    public SampleBeanPostProcessor sampleBPPOrd1(Printer printer) {
        return new SampleBeanPostProcessor(printer, 1);
    }

    @Bean
    public SampleBeanPostProcessor sampleBPPOrd2(Printer printer) {
        return new SampleBeanPostProcessor(printer, 2);
    }

}