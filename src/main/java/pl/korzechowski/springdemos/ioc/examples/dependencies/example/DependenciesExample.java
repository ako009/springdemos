/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.dependencies.example;

import pl.korzechowski.springdemos.framework.*;
import pl.korzechowski.springdemos.ioc.examples.dependencies.beans.DependencyInjectionFieldBean;
import pl.korzechowski.springdemos.ioc.examples.dependencies.configuration.DependenciesConfiguration;

public class DependenciesExample extends BaseExample {

    public DependenciesExample(ExampleConfiguration configuration) {
        super(configuration);
    }

    public static void main(String[] args) {
//        ExampleConfiguration configuration = new XMLConfiguration(
//                "Bean Dependencies via XML Configuration",
//                new LoggerPrinter("XMLAppCtx"),
//                "classpath:context/ioc/dependencies-context-xml.xml");

        ExampleConfiguration configuration = new AnnotationConfiguration(
                "Bean Dependencies via Configuration annotation",
                new LoggerPrinter("AnnotationAppCtx"),
                //BaseBeanPostProcessorConfiguration.class,
                DependenciesConfiguration.class);

        new DependenciesExample(configuration)
            //.setPrintBeansInTheContainer(true, true, true)
            .run();
    }

    @Override
    public void start() {
        getContext().registerShutdownHook();

//        printer.println(">>> Dependency Lookup scenario.");
//        printer.printSeparator();
//        DependencyLookupBean dependencyLookup= getBean("dependencyLookup", DependencyLookupBean.class);
//        dependencyLookup.doSomething();
//        printer.printSeparator();

//        printer.println(">>> Dependency Injection via Constructor scenario.");
//        printer.printSeparator();
//        DependencyInjectionConstructorBean constructorInjection = getBean("constructorInjection", DependencyInjectionConstructorBean.class);
//        constructorInjection.doSomething();
//        printer.printSeparator();
//
//        printer.println(">>> Dependency Injection via Setter scenario.");
//        printer.printSeparator();
//        DependencyInjectionSetterBean setterInjection = getBean("setterInjection", DependencyInjectionSetterBean.class);
//        setterInjection.doSomething();
//        printer.printSeparator();
//
        printer.println(">>> Dependency Injection via Field scenario.");
        printer.printSeparator();
        DependencyInjectionFieldBean fieldInjection = getBean("fieldInjection", DependencyInjectionFieldBean.class);
        fieldInjection.doSomething();
        printer.printSeparator();

    }

}
