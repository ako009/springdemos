/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;

// http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-factory-aware

public class LifecycleBean implements BeanNameAware {

    private final Printer printer;
    private String beanName;
    private static int instanceId = 0;

    @Autowired
    public LifecycleBean(Printer printer) {
        instanceId++;
        this.printer = printer;
        printer.println("LifecycleBean() constructor called! [instanceId=" + instanceId + "].");
    }

    @Override
    public String toString() {
        return "Bean of LifecycleBean type with '" + beanName + "' name. [instanceId=" + instanceId + "]";
    }

    @Override
    public void setBeanName(String name) {
        printer.println("LifecycleBean::setBeanName() called with '" + name + "' name.");
        beanName = name;
    }
}
