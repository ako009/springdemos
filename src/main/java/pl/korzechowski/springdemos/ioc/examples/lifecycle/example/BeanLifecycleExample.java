/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.example;

import pl.korzechowski.springdemos.framework.*;
import pl.korzechowski.springdemos.ioc.examples.lifecycle.configuration.LifecycleBeanConfiguration;
import pl.korzechowski.springdemos.ioc.examples.lifecycle.beans.LifecycleBeanAll;

public class BeanLifecycleExample extends BaseExample {

    public BeanLifecycleExample(ExampleConfiguration configuration) {
        super(configuration);
    }

    public static void main(String[] args) {
//        ExampleConfiguration configuration = new XMLConfiguration(
//                "Bean Lifecycle via XML Configuration",
//                new LoggerPrinter("XMLAppCtx"),
//                //"classpath:context/ioc/postprocessor-context-xml.xml",
//                "classpath:context/ioc/lifecycle-context-xml.xml");

        ExampleConfiguration configuration = new AnnotationConfiguration(
                "Bean Lifecycle via Configuration annotation",
                new LoggerPrinter("AnnotationAppCtx"),
                //BeanPostProcessorConfiguration.class,
                LifecycleBeanConfiguration.class);

        new BeanLifecycleExample(configuration)
            //.setPrintBeansInTheContainer(true, true, true)
            .run();
    }

    @Override
    public void start() {
        // http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-factory-shutdown
        getContext().registerShutdownHook();

        //LifecycleBean lifecycleBean = getBean("lifecycleBean", LifecycleBean.class);
        //LifecycleBean lifecycleBean2 = getBean("lifecycleBean", LifecycleBean.class);
        //LifecycleBean lifecycleBean3 = getBean("lifecycleBean", LifecycleBean.class);

//        LifecycleBeanInterface usingInterface = getBean("usingInterface", LifecycleBeanInterface.class);
        //LifecycleBeanInterface usingInterface2 = getBean("usingInterface", LifecycleBeanInterface.class);
//        LifecycleBeanConfigParam usingConfigBeanParameters = getBean("usingConfigBeanParameters", LifecycleBeanConfigParam.class);
//        LifecycleBeanJSR250 usingJSR250 = getBean("usingJSR250", LifecycleBeanJSR250.class);
        LifecycleBeanAll allInOne = getBean("allInOne", LifecycleBeanAll.class);
//
//        LifecycleBean nonLazyBean = getBean("nonLazyBean", LifecycleBean.class);
//        LifecycleBean lazyBean = getBean("lazyBean", LifecycleBean.class);
    }

}
