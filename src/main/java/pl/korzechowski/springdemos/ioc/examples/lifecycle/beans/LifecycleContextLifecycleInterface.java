/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.Lifecycle;

// http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-factory-lifecycle-processor

public class LifecycleContextLifecycleInterface implements Lifecycle {

    private final Printer printer;
    private boolean isRunning;

    @Autowired
    public LifecycleContextLifecycleInterface(Printer printer) {
        this.printer = printer;
        this.isRunning = false;
        printer.println("LifecycleContextLifecycleInterface() constructor called!");
    }

    @Override
    public String toString() {
        return "Bean of LifecycleContextLifecycleInterface type. [isRunning="+isRunning+"]";
    }

    @Override
    public void start() {
        printer.println("LifecycleContextLifecycleInterface::start() called!");
        isRunning = true;
    }

    @Override
    public void stop() {
        printer.println("LifecycleContextLifecycleInterface::stop() called!");
        isRunning = false;
    }

    @Override
    public boolean isRunning() {
        printer.println("LifecycleContextInterface::isRunning() called! Returns " + isRunning);
        return isRunning;
    }

    public void initMethod() {
        printer.println("LifecycleContextInterface::initMethod() called!");
    }

    public void destroyMethod () {
        printer.println("LifecycleContextInterface::destroyMethod() called!");
    }
}
