/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.dependencies.example;

import pl.korzechowski.springdemos.framework.BaseExample;
import pl.korzechowski.springdemos.framework.ExampleConfiguration;
import pl.korzechowski.springdemos.framework.LoggerPrinter;
import pl.korzechowski.springdemos.framework.AnnotationConfiguration;
import pl.korzechowski.springdemos.ioc.examples.dependencies.beans.WiringDependencyInjectionSetterBean;
import pl.korzechowski.springdemos.ioc.examples.dependencies.configuration.WiringConfiguration;

public class WiringBeansExample extends BaseExample {

    public WiringBeansExample(ExampleConfiguration configuration) {
        super(configuration);
    }

    public static void main(String[] args) {
        ExampleConfiguration configuration = new AnnotationConfiguration(
                "Automatic Wiring Bean Dependencies via Configuration annotation",
                new LoggerPrinter("AnnotationAppCtx"),
                //BaseBeanPostProcessorConfiguration.class,
                WiringConfiguration.class);

        new WiringBeansExample(configuration)
            //.setPrintBeansInTheContainer(true, true, true)
            .run();
    }

    @Override
    public void start() {
        getContext().registerShutdownHook();

        printer.println(">>> Dependency Injection via Setter scenario.");
        printer.printSeparator();
        WiringDependencyInjectionSetterBean wiringSetterBean = getBean("wiringSetterBean",
            WiringDependencyInjectionSetterBean.class);
        wiringSetterBean.doSomething();
        printer.printSeparator();
    }

}
