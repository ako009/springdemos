/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.SmartLifecycle;

// http://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-factory-lifecycle-processor

public class LifecycleContextSmartLifecycleInterface implements SmartLifecycle, BeanNameAware {

    private final Printer printer;
    private boolean isRunning;
    private boolean isAutoStartup;
    private int startupPhase;
    private String beanName;

    @Autowired
    public LifecycleContextSmartLifecycleInterface(Printer printer, int startupPhase, boolean isAutoStartup) {
        this.printer = printer;
        this.isRunning = false;
        this.isAutoStartup = isAutoStartup;
        this.startupPhase = startupPhase;
        printer.println("LifecycleContextSmartLifecycleInterface() constructor called!");
    }

    @Override
    public String toString() {
        return "Bean of LifecycleContextSmartLifecycleInterface type with '" + beanName
                + "' name. [isRunning="+isRunning
                + ", startupPhase="+startupPhase
                + ", isAutoStartup=" + isAutoStartup
                + "]";
    }

    @Override
    public void start() {
        printer.println("LifecycleContextSmartLifecycleInterface::start() called! For bean '" + beanName + "'.");
        isRunning = true;
    }

    @Override
    public void stop() {
        printer.println("LifecycleContextSmartLifecycleInterface::stop() called! For bean '" + beanName + "'.");
        isRunning = false;
    }

    @Override
    public boolean isRunning() {
        printer.println("LifecycleContextSmartLifecycleInterface::isRunning() called! Returns " + isRunning +
                " For bean '" + beanName + "'.");
        return isRunning;
    }

    @Override
    public boolean isAutoStartup() {
        printer.println("LifecycleContextSmartLifecycleInterface::isAutoStartup() called! Returns " + isAutoStartup +
                " For bean '" + beanName + "'.");
        return isAutoStartup;
    }

    @Override
    public void stop(Runnable callback) {
        printer.println("LifecycleContextSmartLifecycleInterface::stop() with callback called! For bean '" + beanName + "'.");
        isRunning = false;
        callback.run();
    }

    @Override
    public int getPhase() {
        printer.println("LifecycleContextSmartLifecycleInterface::getPhase() called! For bean '" + beanName + "'.");
        return startupPhase;
    }

    @Override
    public void setBeanName(String name) {
        printer.println("LifecycleContextSmartLifecycleInterface::setBeanName() called with '" + name + "' name.");
        beanName = name;
    }

    public void initMethod() {
        printer.println("LifecycleContextSmartLifecycleInterface::initMethod() called! [beanName = '" +
            beanName + "'] ");
    }

    public void destroyMethod () {
        printer.println("LifecycleContextSmartLifecycleInterface::destroyMethod() called! [beanName = '" +
            beanName + "'] ");
    }
}
