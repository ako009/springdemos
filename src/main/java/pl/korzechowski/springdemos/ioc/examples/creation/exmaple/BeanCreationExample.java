/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.creation.exmaple;

import pl.korzechowski.springdemos.framework.*;
import pl.korzechowski.springdemos.ioc.examples.creation.beans.CreationBean;
import pl.korzechowski.springdemos.ioc.examples.creation.configuration.CreationExampleConfiguration;

public class BeanCreationExample extends BaseExample {

    public BeanCreationExample(ExampleConfiguration configuration) {
        super(configuration);
    }

    public static void main(String[] args) {
//        ExampleConfiguration configuration = new XMLConfiguration(
//                "Bean Creation via XML Configuration",
//                new StdOutPrinter("XmlAppCtx"),
//                "classpath:context/ioc/creation-context-xml.xml");

        ExampleConfiguration configuration = new AnnotationConfiguration(
                "Bean Creation via Configuration annotation",
                new StdOutPrinter("AnnotationAppCtx"),
                CreationExampleConfiguration.class);

        new BeanCreationExample(configuration)
                .setPrintBeansInTheContainer(true, true)
                .run();
    }

    @Override
    public void start() {
        CreationBean sample = getBean("creationBean", CreationBean.class);
        //CreationBean bean2 = (CreationBean) context.getBean("creationBean-alias-1st");
    }
}
