/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.lifecycle.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.core.Ordered;

//DestructionAwareBeanPostProcessor

public class SampleBeanPostProcessor implements DestructionAwareBeanPostProcessor, Ordered, BeanNameAware {

    private Printer printer;
    private int order;
    private String beanPPName;

    @Autowired
    public SampleBeanPostProcessor(Printer printer, int order) {
        this.printer = printer;
        this.order = order;
        printer.println("SampleBeanPostProcessor() constructor called! [order = " + order + "].");
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        printer.println("-BPP-> SampleBeanPostProcessor::postProcessBeforeInitialization() " +
                "[beanPPName = '" + beanPPName + "'] " +
                "called for '" + beanName + "' bean.");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        printer.println("<-BPP- SampleBeanPostProcessor::postProcessAfterInitialization() " +
                "[beanPPName = '" + beanPPName + "'] " +
                "called for '" + beanName + "' bean.");
        return bean;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public void setBeanName(String name) {
        printer.println("SampleBeanPostProcessor::setBeanName() called with '" + name + "' name.");
        beanPPName = name;
    }

     @Override
     public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
         printer.println("+-BPP- SampleBeanPostProcessor::postProcessBeforeDestruction() " +
                 "[beanPPName = '" + beanPPName + "'] " +
                 "called for '" + beanName + "' bean.");
     }
 }
