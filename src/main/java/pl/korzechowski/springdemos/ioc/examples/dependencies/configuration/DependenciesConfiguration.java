/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.dependencies.configuration;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.korzechowski.springdemos.ioc.examples.dependencies.beans.*;

@Configuration
public class DependenciesConfiguration {

    @Bean
    public ServiceInterface someService(Printer printer) {
        return new SomeServiceBean(printer);
    }

    @Bean
    public DependencyLookupBean dependencyLookup(Printer printer) {
        return new DependencyLookupBean(printer);
    }

    @Bean
    public DependencyInjectionConstructorBean constructorInjection(Printer printer, ServiceInterface someService) {
        return new DependencyInjectionConstructorBean(printer, someService);
    }

    @Bean
    public DependencyInjectionSetterBean setterInjection(Printer printer) {
        return new DependencyInjectionSetterBean(printer);
    }

    @Bean
    public DependencyInjectionFieldBean fieldInjection(Printer printer) {
        return new DependencyInjectionFieldBean(printer);
    }

}
