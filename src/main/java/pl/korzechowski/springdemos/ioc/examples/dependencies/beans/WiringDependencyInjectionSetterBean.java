/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.examples.dependencies.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;

public class WiringDependencyInjectionSetterBean implements BeanNameAware {

    private final Printer printer;
    private String beanName;
    private ServiceInterface service;

    public WiringDependencyInjectionSetterBean(Printer printer) {
        this.printer = printer;
        printer.println("WiringDependencyInjectionSetterBean() constructor called!");
    }

    @Override
    public void setBeanName(String name) {
        printer.println("WiringDependencyInjectionSetterBean::setBeanName() called with '" + name + "' name.");
        beanName = name;
    }

    public void doSomething() {
        printer.println("WiringDependencyInjectionSetterBean::doSomething() called for bean '" + beanName + "'.");
        printer.println("WiringDependencyInjectionSetterBean::doSomething() calling service.performSomeOperation().");
        service.performSomeOperation();
        printer.println("WiringDependencyInjectionSetterBean::doSomething() returned from service.performSomeOperation().");
    }

    @Override
    public String toString() {
        return "Bean of WiringDependencyInjectionSetterBean type.";
    }

    //
    //@Inject
    //@Named("someService")
    @Autowired
    //@Autowired(required = false)
    //@Qualifier(value = "otherService")
    //@Resource
    //@Resource(name = "otherService")
    public void setOtherService(SomeServiceBean otherService) {
        printer.println("WiringDependencyInjectionSetterBean::setService() called for bean '" + beanName + "'.");
        this.service = otherService;
        printer.println("WiringDependencyInjectionSetterBean::setService() service has been injected!");
    }


    // 1) Base type, argument named as base type (different then bean name)
    // public void setSomeService(SomeServiceInterface someServiceInterface) {
    // Result all fails to inject (unless marked somehow @Primary)
    // 2) Base type, argument named as one of the bean name
    // public void setSomeService(SomeServiceInterface service) {
    // Result: all Inject service bean
    // 3) Concrete type, argument named as base type (different then bean name)
    // public void setSomeService(SomeServiceBean someServiceBean) {
    // Result: all Inject SomeServiceBean
    // 4) Base type, argument named as base type (different then bean name)
    // public void setSomeService(SomeServiceInterface someServiceInterface) {
    // use qualifiers, names... @Qualifier("service"), @Qualifier("someServiceXYZ") on bean and component
    // @Resource(name="service")


//    @Autowired and @Inject
//    uses  AutowiredAnnotationBeanPostProcessor
//    Matches by Type
//    Matches by Name

//    @Resource
//    uses  CommonAnnotationBeanPostProcessor
//    Matches by Name
//    Matches by Type

}
