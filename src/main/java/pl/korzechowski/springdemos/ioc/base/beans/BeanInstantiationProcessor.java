/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.ioc.base.beans;

import pl.korzechowski.springdemos.framework.Printer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.core.Ordered;

public class BeanInstantiationProcessor extends InstantiationAwareBeanPostProcessorAdapter implements Ordered {

    private Printer printer;

    @Autowired
    public BeanInstantiationProcessor(Printer printer) {
        this.printer = printer;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        printer.println("-BPP-> BeanInstantiationProcessor::postProcessBeforeInitialization() called for '" + beanName + "' bean.");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        printer.println("<-BPP- BeanInstantiationProcessor::postProcessAfterInitialization() called for '" + beanName + "' bean.");
        return bean;
    }

    @Override
    public int getOrder() {
        return 0;
    }


    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        printer.println("-BPP-* BeanInstantiationProcessor::postProcessBeforeInstantiation() called for '" + beanName + "' bean.");
        return null;
    }

    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        printer.println("*-BPP- BeanInstantiationProcessor::postProcessAfterInstantiation() called for '" + beanName + "' bean.");
        return true;
    }
}
