/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.framework;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.GenericApplicationContext;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class BaseExample implements Example {

    private boolean printBeansInTheContainer = false;
    private boolean includeDetailsForPrintedBeans = false;
    private boolean includeDependenciesForPrintedBeans = false;

    private final ExampleConfiguration configuration;
    protected Printer printer;
    protected GenericApplicationContext context;

    public BaseExample(ExampleConfiguration configuration) {
        this.configuration = configuration;
        this.printer = configuration.getPrinter();
    }

    @Override
    public GenericApplicationContext getContext() {
        return context;
    }

    @Override
    public void run() {
        printHeader(configuration.getTitle());
        context = configuration.createApplicationContext();
        refresh(context);
        start();
        printFooter();
    }

    public abstract void start();

    private void printHeader(String title) {
        printer.println("*****************************************************************************************");
        printer.println("* Executing '" + title + "' example.");
        printer.println("*****************************************************************************************");
    }

    private void printFooter() {
        printer.println("*****************************************************************************************");
        printer.println("* End of example execution.");
        printer.println("*****************************************************************************************");
    }

    public void refresh(GenericApplicationContext context) {
        printer.println("Refreshing context...");
        context.refresh();
        printer.println("DONE. Context has been refreshed.");
        printer.printSeparator();
        if (printBeansInTheContainer) {
            printBeansInTheContainer(context, includeDetailsForPrintedBeans, includeDependenciesForPrintedBeans);
        }
    }

    public void printBeansInTheContainer(boolean includeDetails, boolean includeDependencies) {
        printBeansInTheContainer(context, includeDetails, includeDependencies);
    }

    private void printBeansInTheContainer(GenericApplicationContext context, boolean includeDetails,
                                          boolean includeDependencies) {
        printer.println("***** List of bean definitions in the container:");
        ConfigurableListableBeanFactory beanFactory = context.getBeanFactory();
        String[] beanNames = context.getBeanDefinitionNames();
        for (int i = 0; i < beanNames.length; i++) {
            String beanName = beanNames[i];
            printer.println(String.format("%d", i+1) + ") Bean '" + beanName+"'");

            if (includeDetails) {
                BeanDefinition beanDefinition = context.getBeanDefinition(beanName);
                printer.println("  Details: " + beanDefinition);
                printBeanFromArray("  Aliases: ", context.getAliases(beanName));
            }

            if (includeDependencies) {
                printBeanFromArray("  Dependant: ", beanFactory.getDependentBeans(beanName));
                printBeanFromArray("  Dependencies: ", beanFactory.getDependenciesForBean(beanName));
            }

        }
        printer.println("***** End of List of bean definitions in the container.");
        printer.printSeparator();
    }

    private void printBeanFromArray(String rowHeader, String[] arrayOfBeans) {
        Optional<String> beans = Optional.empty();

        if (arrayOfBeans.length > 0) {
            beans = Optional.of(Arrays.stream(arrayOfBeans).map(x -> "'" + x + "'")
                    .collect(Collectors.joining(", ")));
        }
        printer.println(rowHeader + beans.orElse("NONE"));
    }

    @Override
    public <T> T getBean(String name, Class<T> requiredType) {
        printer.println("Retrieving bean '" + name + "' of " + requiredType + "...");
        T bean = context.getBean(name, requiredType);
        printer.println("DONE. Bean has been retrieved.");
        printer.println("Bean [" + name + "] toString='" + String.valueOf(bean) + "'.");
        printer.printSeparator();
        return bean;
    }

    public Example setPrintBeansInTheContainer(boolean printBeansInTheContainer) {
        return setPrintBeansInTheContainer(printBeansInTheContainer, false, false);
    }

    public Example setPrintBeansInTheContainer(boolean printBeansInTheContainer,
                                           boolean includeDetailsForPrintedBeans) {
        return setPrintBeansInTheContainer(printBeansInTheContainer, includeDetailsForPrintedBeans, false);
    }

    public Example setPrintBeansInTheContainer(boolean printBeansInTheContainer,
                                               boolean includeDetailsForPrintedBeans,
                                               boolean includeDependenciesForPrintedBeans) {
        this.printBeansInTheContainer = printBeansInTheContainer;
        this.includeDetailsForPrintedBeans = includeDetailsForPrintedBeans;
        this.includeDependenciesForPrintedBeans = includeDependenciesForPrintedBeans;
        return this;
    }

}
