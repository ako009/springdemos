/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.framework;

public abstract class BaseExampleConfiguration implements ExampleConfiguration {

    protected final Printer printer;
    private final String title;

    public BaseExampleConfiguration(String title, Printer printer) {
        this.title = title;
        this.printer = printer;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Printer getPrinter() {
        return printer;
    }
}
