/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerPrinter extends BasePrinter {

    Logger logger = LoggerFactory.getLogger(LoggerPrinter.class);

    public LoggerPrinter(String prefix) {
        setPrefix(prefix);
    }

    @Override
    public void println(String message) {
        logger.info(getPrefix() + message);
    }

}