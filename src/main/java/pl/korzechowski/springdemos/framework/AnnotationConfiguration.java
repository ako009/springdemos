/*
 * Copyright 2015 Krystian Orzechowski
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.korzechowski.springdemos.framework;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.util.Arrays;

public class AnnotationConfiguration extends BaseExampleConfiguration {

    private final Class<?>[] annotatedClasses;

    public AnnotationConfiguration(String title, Printer printer, Class<?>... annotatedClasses) {
        super(title, printer);
        this.annotatedClasses = annotatedClasses;
    }

    @Override
    public GenericApplicationContext createApplicationContext() {
        printer.println("Creating AnnotationConfigApplicationContext...");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        printer.println("DONE. Context has been created.");
        printer.printSeparator();

        printer.println("Registering configuration classes...");
        printer.println("classes=" + Arrays.asList(annotatedClasses));
        context.register(BaseConfiguration.class);
        context.register(annotatedClasses);
        printer.println("DONE. Registration has been finished.");
        printer.printSeparator();

        return context;
    }

}
